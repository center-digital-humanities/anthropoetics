<?php
/*
 Template Name: Home Page
*/
?>
<?php get_header(); 
	if(get_field('additional_information')) {
		$additional_information = get_field('additional_information');
	}
?>
			<div id="main-content" role="main">
				<div class="content">
					
					<div class="row">
						<div class="left-col">
							<?php if(get_field('left_image')) { ?>
								<a href="<?php the_field('left_image_link'); ?>"><img src="<?php the_field('left_image'); ?>" alt="Anthropoetics Journal of Generative Anthropology" /></a>
							<?php } ?>
						</div>
						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						<div class="center-col">
							<?php the_content(); ?>
						</div>
						<?php endwhile; else : endif; ?>
						<div class="right-col">
							<?php if(get_field('right_image')) { ?>
								<img id="chronicles-cover" src="<?php the_field('right_image'); ?>" border="0" width="225" height="300" orgWidth="225" orgHeight="300" usemap="#chronicles-cover" />
								<map name="chronicles-cover" id="chronicles-cover">
								<area  alt="Chronicles of Love & Resentment" title="Chronicles of Love & Resentment" href="<?php the_field('right_image_link'); ?>" shape="rect" coords="0,3,225,166" style="outline:none;" target="_self" />
								<?php query_posts( 'category_name=views&posts_per_page=1' ); ?>
								<?php while ( have_posts() ) : the_post(); ?>
									<area  alt="Latest Chronicle" title="Latest Chronicle" href="<?php the_permalink() ?>" shape="rect" coords="0,167,225,300" style="outline:none;" target="_self" />
								<?php endwhile; 
								wp_reset_postdata(); ?>
								</map>
							<?php } ?>
						</div>
					</div>
					
					<div class="row">
						<div class="left-col">
							<h2>Navigation</h2>
							<nav role="navigation" aria-labelledby="main navigation" class="desktop">
								<?php wp_nav_menu(array(
									'container' => false,
									'menu' => __( 'Main Menu', 'bonestheme' ),
									'menu_class' => 'main-nav',
									'theme_location' => 'main-nav',
									'before' => '',
									'after' => '',
									'depth' => 1,
								)); ?>
							</nav>
							<?php // get_search_form(); ?>
						</div>
						<div class="center-col">
						<?php echo $additional_information; ?>
							
						</div>
						<div class="right-col">
							<h2>Communications</h2>
							<nav role="navigation" aria-labelledby="main navigation" class="desktop">
								<?php wp_nav_menu(array(
									'container' => false,
									'menu' => __( 'Communications Menu', 'bonestheme' ),
									'menu_class' => 'communications-nav',
									'theme_location' => 'communications-nav',
									'before' => '',
									'after' => '',
									'depth' => 1,
								)); ?>
							</nav>
							<div class="facebook">
								<a href="http://www.facebook.com/pages/Anthropoetics-Originary-Thinking-and-Generative-Anthropology/336859615577"><img src="<?php echo get_template_directory_uri(); ?>/library/images/facebook-badge.jpg" alt="Find us on Facebook" /></a>
							</div>
						</div>
					</div>
					
				</div>
			</div>
<?php get_footer(); ?>