<?php
/*
 Template Name: Anthropoetics Article List
*/
?>
<?php get_header(); ?>

			<div class="content">
				<div id="main-content" role="main">
					<h1><?php the_title(); ?></h1>
					<ul <?php post_class('cf'); ?>>
					<?php $anthro_loop = new WP_Query( 
						array( 'cat' => '-1,-5,-6,', 'posts_per_page' => -1, 'orderby' => 'meta_value', 'meta_key' => 'sortable_name', 'order' => 'ASC',
							// Hide post without an author
							'meta_query' => array(
								array(
									'key'     => 'author',
									'value'   => '',
									'compare' => '!='
								)
							)
						)
					); ?>
					<li>
						<div class="column"><strong>Author</strong></div>
						<div class="column"><strong>Title</strong></div>
						<div class="column"><strong>Issue</strong></div>
					</li>
					<?php while ( $anthro_loop->have_posts() ) : $anthro_loop->the_post(); ?>
						<li>
							<?php if(get_field('author')) { ?>
								<div class="column"><?php the_field('author'); ?></div>
							<?php } ?>
							<div class="column"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></div>
							<div class="column"><?php the_category( ' ' ); ?></div>
						</li>
					<?php endwhile; ?>
					</ul>
				</div>
			</div>

<?php get_footer(); ?>