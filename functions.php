<?php
// Load Bones Core
require_once( 'library/bones.php' );
	
// Customize Wordpress Admin
require_once( 'library/admin.php' );

// Recommend Plugins
require_once( 'library/install-plugins.php' );

/*********************
LAUNCH BONES
*********************/

function bones_ahoy() {

	// Allow editor style.
	add_editor_style( 'library/css/editor-style.css');

/************* END *************/

	// launching operation cleanup
	add_action( 'init', 'bones_head_cleanup' );
	// A better title
	add_filter( 'wp_title', 'rw_title', 10, 3 );
	// remove WP version from RSS
	add_filter( 'the_generator', 'bones_rss_version' );
	// clean up gallery output in wp
	add_filter( 'gallery_style', 'bones_gallery_style' );
	// enqueue base scripts and styles
	add_action( 'wp_enqueue_scripts', 'bones_scripts_and_styles', 999 );
	// launching this stuff after theme setup
	bones_theme_support();
	// adding sidebars to Wordpress (these are created in functions.php)
	add_action( 'widgets_init', 'bones_register_sidebars' );
	// cleaning up random code around images
	add_filter( 'the_content', 'bones_filter_ptags_on_images' );
} /* end bones ahoy */

// let's get this party started
add_action( 'after_setup_theme', 'bones_ahoy' );


/************* OEMBED SIZE OPTIONS *************/

if ( ! isset( $content_width ) ) {
	$content_width = 640;
}

/************* THUMBNAIL SIZE OPTIONS *************/

// Content width
add_image_size( 'content-width', 640, 430, true );
// Big people photo
add_image_size( 'people-large', 280, 280, array( 'center', 'top' ) );
// Small people photo
add_image_size( 'people-thumb', 100, 100, array( 'center', 'top' ) );

/* 
This makes Wordpress create thumbnails at these 
sizes for every image for you to then use in
theme as needed.

To add more sizes, simply copy a line and change 
the dimensions & name. As long as you upload a 
"featured image" as large as the biggest set 
width or height, all the other sizes will be
auto-cropped.

To call a different size, simply change the text
inside the thumbnail function.

For example, to call the 300 x 100 sized image,
we would use the function:
<?php the_post_thumbnail( 'bones-thumb-300' ); ?>
*/

add_filter( 'image_size_names_choose', 'bones_custom_image_sizes' );

function bones_custom_image_sizes( $sizes ) {
	return array_merge( $sizes, array(
		'content-width' => __('Full Content Width'),
	) );
}

/*
The function above adds the ability to use the dropdown menu to select
the new images sizes you have just created from within the media manager
when you add media to your content blocks. If you add more image sizes,
duplicate one of the lines in the array and name it according to your
new image size.
*/

/************* SIDEBARS ********************/

// Sidebars & Widgetizes Areas
function bones_register_sidebars() {
	register_sidebar(array(
		'id' => 'news-sidebar',
		'name' => __( 'News Sidebar', 'bonestheme' ),
		'description' => __( 'Sidebar for news article feed.', 'bonestheme' ),
		'before_widget' => '<div class="widget %2$s">',
		'after_widget' => '</div><a class="btn" href="/category/news/">View All<span class="hidden"> News</span></a>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));
	
	register_sidebar(array(
		'id' => 'events-sidebar',
		'name' => __( 'Event Sidebar', 'bonestheme' ),
		'description' => __( 'Sidebar for event feed.', 'bonestheme' ),
		'before_widget' => '<div class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));

} // don't remove this bracket!

/*********************
ADDITIONAL FUNCTIONS
*********************/

function prefix_nav_description( $item_output, $item, $depth, $args ) {
    if ( !empty( $item->description ) ) {
        $item_output = str_replace( $args->link_after . '</a>', '<span class="description">' . $item->description . '</span>' . $args->link_after . '</a>', $item_output );
    }
 
    return $item_output;
}
add_filter( 'walker_nav_menu_start_el', 'prefix_nav_description', 10, 4 );

/************* Filter Menu *********************/

// Removing classes and replacing anchor with button element

class Filter_Walker extends Walker_Nav_Menu {
	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "\n$indent\n";
	}
	function end_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "$indent\n";
	}
	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
		$class_names = $value = '';
		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
		$class_names = $class_names ? ' class="option ' . esc_attr( $class_names ) . '"' : '';
		$output .= $indent . '';
		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		$attributes .= ' data-filter=".'. esc_attr( $item->xfn   ) .'"';
		$attributes .= ' data-text="'. esc_attr( $item->title   ) .'"';
		$item_output = $args->before;
		$item_output .= '<button'. $attributes . $class_names .'>';
		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
		$item_output .= '</button>';
		$item_output .= $args->after;
		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
	function end_el( &$output, $item, $depth = 0, $args = array() ) {
		$output .= "\n";
	}
}

// Removing all but custom classes
function modify_nav_menu_args( $args ) {
	if( 'faculty-filter' == $args['theme_location'] ) {
		add_filter('nav_menu_css_class', 'discard_menu_classes', 10, 2);
		function discard_menu_classes($classes, $item) {
			return (array)get_post_meta( $item->ID, '_menu_item_classes', true );
		}
	}
	return $args;
}
add_filter( 'wp_nav_menu_args', 'modify_nav_menu_args' );

// Converting menu to drop down
class Dropdown_Walker extends Walker_Nav_Menu {
	function start_lvl(&$output, $depth = 0, $args = array()){
		$indent = str_repeat("\t", $depth); // don't output children opening tag (`<ul>`)
	}
	function end_lvl(&$output, $depth = 0, $args = array()){
		$indent = str_repeat("\t", $depth); // don't output children closing tag
	}
	function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
 		$xfn = '#' !== $item->xfn ? $item->xfn : '';
 		$output .= '<option value=".' . $xfn . '">' . $item->title;
	}	
	function end_el(&$output, $item, $depth = 0, $args = array()){
		$output .= "</option>\n"; // replace closing </li> with the option tag
	}
}

/************* TARGETING PARENT PAGE *********************/

// Be able to target pages by using is_tree(parent-id)

function is_tree($pid) {
	global $post;
	
	$ancestors = get_post_ancestors($post->$pid);
	$root = count($ancestors) - 1;
	$parent = $ancestors[$root];
	
	if(is_page() && (is_page($pid) || $post->post_parent == $pid || in_array($pid, $ancestors))) {
		return true;
	}
	else {
		return false;
	}
};

/************* SHORTCODE *********************/

// Remove bad html in shortcode
function shortcode_empty_paragraph_fix($content) {
	$array = array (
		'<p>[' => '[',
		']</p>' => ']',
		']<br />' => ']'
	);
	$content = strtr($content, $array);
	return $content;
}
add_filter('the_content', 'shortcode_empty_paragraph_fix');

// FAQ Container
function faq_shortcode( $atts, $content = null ) {
	return '<dl class="faq">' . do_shortcode($content) . '</dl>';
}
add_shortcode( 'faq', 'faq_shortcode' );

// Question Shortcode
function question_shortcode( $atts, $content = null ) {
	extract( shortcode_atts(
		array(
			'title' => '',
		),
	$atts ) );
	return '<dt class="question">'. $title .'</dt><dd class="answer">' . do_shortcode($content) . '</dd>';
}
add_shortcode('question', 'question_shortcode');

/* Code Example
[faq]
[question title="To be or not to be?"]That is the question.[/question]
[/faq]
*/

/************* HIDING SPECIFIC PAGES FROM INTERNAL SEARCH *********************/

/*
function search_filter( $query ) {
	if ( $query->is_search && $query->is_main_query() ) {
		$query->set( 'post__not_in', array( 626,617 ) ); 
	}
}
add_filter( 'pre_get_posts', 'search_filter' );
*/

/************* MAKING IMAGE CAPTIONS IN HTML5 *********************/

add_filter( 'img_caption_shortcode', 'cleaner_caption', 10, 3 );

function cleaner_caption( $output, $attr, $content ) {

	/* We're not worried abut captions in feeds, so just return the output here. */
	if ( is_feed() )
		return $output;

	/* Set up the default arguments. */
	$defaults = array(
		'id' => '',
		'align' => 'alignnone',
		'width' => '',
		'caption' => ''
	);

	/* Merge the defaults with user input. */
	$attr = shortcode_atts( $defaults, $attr );

	/* If the width is less than 1 or there is no caption, return the content wrapped between the [caption]< tags. */
	if ( 1 > $attr['width'] || empty( $attr['caption'] ) )
		return $content;

	/* Set up the attributes for the caption <div>. */
	$attributes = ( !empty( $attr['id'] ) ? ' id="' . esc_attr( $attr['id'] ) . '"' : '' );
	$attributes .= ' class="wp-caption ' . esc_attr( $attr['align'] ) . '"';
	$attributes .= ' style="width: ' . esc_attr( $attr['width'] ) . 'px"';

	/* Open the caption <div>. */
	$output = '<figure' . $attributes .'>';

	/* Allow shortcodes for the content the caption was created for. */
	$output .= do_shortcode( $content );

	/* Append the caption text. */
	$output .= '<figcaption class="wp-caption-text">' . $attr['caption'] . '</figcaption>';

	/* Close the caption </div>. */
	$output .= '</figure>';

	/* Return the formatted, clean caption. */
	return $output;
}

/************* SETTING EXCERPT STRING *********************/

function new_excerpt_more( $more ) {
	return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');

function page_title($form){
	$currenttitle = get_the_title();
	return $currenttitle;
}
add_filter("gform_field_value_page_title", "page_title");

/************* RSS FEED EDITS *********************/

function chronical_rss($content) {  
    if(is_feed()) {  
        $post_id = get_the_ID();  
        
        $issue_number = get_field('issue_number');

            if( !empty($issue_number) ): 
                $output .=  $issue_number;
                $output .= ' - ';
            endif; 
  
        $content = $output.$content; 
    }  
    return $content;  
}  
add_filter('the_content','chronical_rss');

/************* ADDING ID TO WP COLUMN *********************/

/**
 * Just adds a column without content
 *
 * @param array $columns Array of all the current columns IDs and titles
 */
function misha_add_column( $columns ){
	$columns['misha_post_id_clmn'] = 'ID'; // $columns['Column ID'] = 'Column Title';
	return $columns;
}
add_filter('manage_posts_columns', 'misha_add_column', 5);
//add_filter('manage_pages_columns', 'misha_add_column', 5); // for Pages
 
 
/**
 * Fills the column content
 *
 * @param string $column ID of the column
 * @param integer $id Post ID
 */
function misha_column_content( $column, $id ){
	if( $column === 'misha_post_id_clmn')
		echo $id;
}
add_action('manage_posts_custom_column', 'misha_column_content', 5, 2);
//add_action('manage_pages_custom_column', 'misha_column_content', 5, 2); // for Pages

/* DON'T DELETE THIS CLOSING TAG */ ?>