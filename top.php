<?php
/*
	Basic Navigation and Hero 
*/
?>
<a href="#main-content" class="hidden skip">Skip to main content</a>

<?php if ( in_category( 'views' ) ) { ?>
<div id="container" class="views">
	<header role="banner" class="top">
		<div class="content">
			<a href="/views">
				<h2><img src="<?php echo get_template_directory_uri(); ?>/library/images/chronicles-logo.png" alt="Chronicles of Love & Resentment by Eric Gans" class="logo" /><span class="hidden">Chronicles of Love & Resentment by Eric Gans</span></h2>
			</a>
		</div>
	</header>

<?php } else { ?>
<div id="container">
	<header role="banner" class="top">
		<div class="content">
			<a href="<?php echo home_url(); ?>" rel="nofollow">
				<?php // H1 if homepage, H2 otherwise.
					if ( is_front_page() ) { ?>
				<h1><img src="<?php echo get_template_directory_uri(); ?>/library/images/anthro-logo.png" alt="<?php the_field('department_name', 'option'); ?>" class="logo" /><span class="hidden"><?php the_field('department_name', 'option'); ?></span></h1>
				<?php } else { ?>
				<h2><img src="<?php echo get_template_directory_uri(); ?>/library/images/anthro-logo.png" alt="<?php the_field('department_name', 'option'); ?>" class="logo" /><span class="hidden"><?php the_field('department_name', 'option'); ?></span></h2>
				<?php } ?>
			</a>
		</div>
	</header>
<?php } ?>
	<?php if ( is_front_page() ) { ?> 
	<section class="subscribe">
		<ul>
			<li>
				<div class="social">
					<a href="http://www.addthis.com/bookmark.php?v=250&amp;username=xa-4cbe6ce5170394e7" class="addthis_button_compact"> Share</a>
					<a class="addthis_button_preferred_1"></a>
					<a class="addthis_button_preferred_2"></a>
					<a class="addthis_button_preferred_3"></a>
					<a class="addthis_button_preferred_4"></a>
				</div>
			</li>
			<li>
				<img src="<?php echo get_template_directory_uri(); ?>/library/images/blue-rss.png" alt="RSS feed icon" height="12" width="12"> <strong><a href="http://news.anthropoetics.ucla.edu/feed/">Subscribe to GA News Feed</a></strong>
			</li>
			<li>
				<img src="<?php echo get_template_directory_uri(); ?>/library/images/green-rss.png" height="12" width="12"> <strong><a href="http://gablog.cdh.ucla.edu/feed/">Subscribe to GABlog posts</a></strong>
			</li>
			<li>
				<img src="<?php echo get_template_directory_uri(); ?>/library/images/lightblue-rss.png" alt="" width="12" height="12"><strong> <a href="/galist">Subscribe to GAlist</a></strong>
			</li>
			<li>
				<img src="<?php echo get_template_directory_uri(); ?>/library/images/twitter.png" alt="Anthropoetics on Twitter" width="16" height="16"> <a href="http://www.twitter.com/Anthropoetics"><strong><em>Anthropoetics</em> on Twitter</strong></a>
			</li>
			<li>
				<img src="<?php echo get_template_directory_uri(); ?>/library/images/orange-rss.png" height="12" width="12"><strong> <a href="/category/views/feed/">Subscribe to Chronicles RSS</a></strong>
			</li>
			<li>
				<img src="<?php echo get_template_directory_uri(); ?>/library/images/purple-rss.png" height="12" width="12"><strong> <a href="/feed/?cat=-5">Subscribe to Anthropoetics RSS</a></strong>
			</li>
			<li>
				<img src="<?php echo get_template_directory_uri(); ?>/library/images/brown-rss.png" height="12" width="12"><a href="http://www.librarything.com/rss/recent/Anthropoetics"> Subscribe to LibraryThing</a></strong>
			</li>
		</ul>
	</section>
	<?php } ?>
	
	