<?php get_header(); ?>

			<div class="content">
				<div id="main-content" role="main">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<div class="social">
							<a href="http://www.addthis.com/bookmark.php?v=250&amp;username=xa-4cbe6ce5170394e7" class="addthis_button_compact"> Share</a> 
							<a class="addthis_button_preferred_1"></a>
							<a class="addthis_button_preferred_2"></a>
							<a class="addthis_button_preferred_3"></a>
							<a class="addthis_button_preferred_4"></a> 
							| <img src="<?php echo get_template_directory_uri(); ?>/library/images/orange-rss.png" height="12" width="12"><strong> <a href="/category/views/feed/">Subscribe to Chronicles RSS</a></strong>
						</div>
						<div class="article-info">
							<?php // If in Chronicles, do this.
							if (in_category('views')) { ?>
							<div class="chronicle-title">
								<div class="left-col">
									<img src="<?php echo get_template_directory_uri(); ?>/library/images/x-black.png" height="168" width="100">
								</div>
								<div class="center-col">
									<h1 class="entry-title single-title" itemprop="headline"><?php the_title(); ?></h1>
								</div>
								<div class="right-col">
									<img src="<?php echo get_template_directory_uri(); ?>/library/images/x-white.png" height="168" width="100">
								</div>
							</div>
							
							<?php } else // if not in Chronicles, do this.
							{ ?>
								<div class="category"><?php the_category( ' ' ); ?></div>
								<h1 class="entry-title single-title" itemprop="headline"><?php the_title(); ?></h1>
							<?php } // End conditional
							?>
							<?php if(get_field('author')) { ?>
								<h2><?php the_field('author'); ?></h2>
							<?php } ?>
							<?php if(get_field('issue_number')) { ?>
								<h4><?php the_field('issue_number'); ?>: <?php the_date('l, F jS, Y'); ?></h4>
							<?php } ?>
						</div>
						<section class="entry-content cf" itemprop="articleBody">
							<?php the_content(); ?>
							
							<?php if (in_category('views')) {
								gravity_form('Post Feedback', false, false, false, '', false);	
							} ?>
							
						</section>
					</article>

				<?php endwhile; else : ?>

					<article id="post-not-found" <?php post_class( 'cf' ); ?> role="article">
						<h1>Page Not Found</h1>
						<section>
							<p>Sorry but the page you are looking for is not here. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
						</section>
					</article>

				<?php endif; ?>
				
				</div>
			</div>

<?php get_footer(); ?>