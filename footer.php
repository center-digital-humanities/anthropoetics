			<footer role="contentinfo">
				<div class="content">
					<?php if ( is_front_page() ) { ?>
					<div class="row">
						<div class="left-col">
							<a href="http://dev.anthropoetics.ucla.edu/archive/">Anthropoetics/Chronicles archive</a>
						</div>
						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						<div class="center-col">
							<a href="http://www.anthropoetics.ucla.edu/hogb.html">Motto <img src="<?php echo get_template_directory_uri(); ?>/library/images/hog.gif" alt="Mascot" /> Mascot</a>
						</div>
						<?php endwhile; else : endif; ?>
						<div class="right-col">
							<a href="http://dev.anthropoetics.ucla.edu/feedbk/">Feedback</a>
						</div>
					</div>
					<?php } ?>
					<?php if (in_category('views')) { ?>
						<?php if (is_single()) { ?>
						<p class="views-footer">If you enjoyed this column, subscribe to the <a href="/galist">GAlist</a> and you'll receive updates about our on-line journal <a href="/anthro">Anthropoetics</a> and the <strong>Chronicles of Love and Resentment</strong> regularly by email.</p>
						<?php } ?>
						<p><a href="/views">Chronicles home page</a> | <a href="/">Return to Anthropoetics home page</a> | <a href="/feedback">Chronicles Feedback</a></p>
						<div class="copyright">
							<a href="http://www.french.ucla.edu/faculty/gans/index.html">Eric Gans</a> / <a href="mailto:gans@humnet.ucla.edu">gans@humnet.ucla.edu</a><br />
							Copyright <?php echo date('Y'); ?> Chronicles of Love & Resentment 
						</div>
					<?php } else { ?>
					<div class="copyright">
						<a href="mailto:ap@humnet.ucla.edu">ap@humnet.ucla.edu</a><br />
						Last updated: <?php the_modified_time('F j, Y'); ?> at <?php the_modified_time('g:i a'); ?><br />
						Copyright <?php echo date('Y'); ?> Anthropoetics 
					</div>
					<?php } ?>
				</div>
			</footer>
		<?php wp_footer(); ?>
	</body>
</html>