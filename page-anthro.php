<?php
/*
 Template Name: Anthropoetics Archive
*/
?>
<?php get_header(); ?>

			<div class="content">
				<div id="main-content" role="main">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
					
						<section>
							<?php the_content(); ?>
							<?php $category = get_field('current_issue');
								$category_id = $category->term_id;
								if( $category ): ?>
								
								<?php $current_issue = new WP_Query( 
									array( 'cat' => "'$category_id'", 'posts_per_page' => -1, 
									)
								);
							?>
							<h2>Current Issue</h2>
							<h3><a href="/<?php echo $category->slug; ?>"><?php echo $category->name; ?></a></h3>
							
							<ul>
								<?php if ( $current_issue->have_posts() ) : while ( $current_issue->have_posts() ) : $current_issue->the_post(); ?>
								<li>
									<?php if(get_field('author')) { ?><strong><?php the_field('author'); ?></strong> - <?php } ?><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a>
									
									<?php if(get_field('pdf')) { ?>
										<a href="<?php the_field('pdf'); ?>" >
									<?php } else { ?>
										<a class="wpptopdfenh" title="Download PDF" href="<?php the_permalink() ?>?pdf=<?php the_id(); ?>" target="_blank" rel="noindex,nofollow">
									<?php } ?>									
									<img src="<?php echo get_template_directory_uri(); ?>/library/images/pdf-icon.png" alt="Download PDF" /></a>
								</li>
								<?php endwhile;?>
							</ul> 
								<?php else : endif; ?>
								<?php wp_reset_postdata(); ?>
							<?php endif; ?>
						</section>
						<section class="subscribe">
							<ul>
								<li>
									<div class="social">
										<a href="http://www.addthis.com/bookmark.php?v=250&amp;username=xa-4cbe6ce5170394e7" class="addthis_button_compact"> Share</a>
										<span class="addthis_separator">|</span>
										<a class="addthis_button_preferred_1"></a>
										<a class="addthis_button_preferred_2"></a>
										<a class="addthis_button_preferred_3"></a>
										<a class="addthis_button_preferred_4"></a>
									</div>
								</li>
								<li class="email">
									<img src="<?php echo get_template_directory_uri(); ?>/library/images/mail-icon.png" alt="Anthropoetics Email Subscription" /> <a href="http://feedburner.google.com/fb/a/mailverify?uri=AnthropoeticsTheJournalOfGenerativeAnthropology&amp;loc=en_US"><em>Anthropoetics</em> Email Subscription</a>
								</li>
								<li class="rss-library">
									<img src="<?php echo get_template_directory_uri(); ?>/library/images/brown-rss.png" height="12" width="12"> <a href="http://www.librarything.com/rss/recent/Anthropoetics"> Subscribe to LibraryThing</a></strong>
								</li>
								<li>
									<img src="<?php echo get_template_directory_uri(); ?>/library/images/anthro-home.png" width="16" height="15" /> <a href="<?php echo home_url(); ?>"><em>Anthropoetics</em> Home</a>
								</li>
								<li class="twitter">
									<img src="<?php echo get_template_directory_uri(); ?>/library/images/twitter.png" alt="Anthropoetics on Twitter" width="16" height="16"> <a href="http://www.twitter.com/Anthropoetics"><em>Anthropoetics</em> on Twitter</a>
								</li>
								<li class="anthro-rss">
									<img src="<?php echo get_template_directory_uri(); ?>/library/images/purple-rss.png" height="12" width="12"> <a href="/feed/">Subscribe to <em>Anthropoetics</em> RSS</a>
								</li>
								<li class="ga-rss">
									<img src="<?php echo get_template_directory_uri(); ?>/library/images/blue-rss.png" alt="RSS feed icon" height="12" width="12"> <a href="http://news.anthropoetics.ucla.edu/feed/">Subscribe to GA News Feed</a>
								</li>
							</ul>
						</section>
						<nav role="navigation" aria-labelledby="navigation" class="anthro-nav">
							<?php wp_nav_menu(array(
								'container' => false,
								'menu' => __( 'Anthropoetics Menu', 'bonestheme' ),
								'menu_class' => 'anthropoetics-nav',
								'theme_location' => 'anthropoetics-nav',
								'before' => '',
								'after' => '',
								'depth' => 1,
							)); ?>
						</nav>
						<section class="past-issues">
							<?php the_field('past_issues'); ?>
						</section>
					</article>

				<?php endwhile; else : endif; ?>

				</div>
			</div>

<?php get_footer(); ?>