<?php get_header(); ?>
			<div class="content main">
				<div id="main-content" role="main">

					<?php if (is_category()) { ?>
					
					<?php } elseif (is_tag()) { ?>
					<h1 class="archive-title">
						<span><?php _e( 'Articles Tagged:', 'bonestheme' ); ?></span> <?php single_tag_title(); ?>
					</h1>
					<?php } elseif (is_author()) {
						global $post;
						$author_id = $post->post_author;
					?>
					<h1 class="archive-title">
						<span><?php _e( 'Articles By:', 'bonestheme' ); ?></span> <?php the_author_meta('display_name', $author_id); ?>
					</h1>
					<?php } elseif (is_day()) { ?>
					<h1 class="archive-title">
						<span><?php _e( 'Daily Archives:', 'bonestheme' ); ?></span> <?php the_time('l, F j, Y'); ?>
					</h1>
					<?php } elseif (is_month()) { ?>
					<h1 class="archive-title">
						<span><?php _e( 'Monthly Archives:', 'bonestheme' ); ?></span> <?php the_time('F Y'); ?>
					</h1>
					<?php } elseif (is_year()) { ?>
					<h1 class="archive-title">
						<span><?php _e( 'Yearly Archives:', 'bonestheme' ); ?></span> <?php the_time('Y'); ?>
					</h1>
					<?php } ?>
					<?php if (in_category('views')) { ?>
					
					<img src="<?php echo get_template_directory_uri(); ?>/library/images/orange-rss.png" height="12" width="12"><strong> <a href="/category/views/feed/">Subscribe to Chronicles RSS</a></strong>
					<div class="article-info">
						<div class="chronicle-title">
							<div class="left-col">
								<img src="<?php echo get_template_directory_uri(); ?>/library/images/x-black.png" height="168" width="100">
							</div>
							<div class="center-col">
								<h4>Latest Chronicle</h4>
								<br />
						<?php
						$args = array( 'numberposts' => '1', 'category_name' => 'views' );
						$recent_posts = wp_get_recent_posts( $args );
						foreach( $recent_posts as $recent ){ ?>
								<h3><a href="<?php the_permalink() ?>" rel="bookmark"><?php if(get_field('issue_number')) { ?><?php the_field('issue_number'); ?>,<?php } ?> <?php echo get_the_date(); ?><br /><br />
								<?php the_title(); ?></a></h3>
								
						<?php } ?>
							</div>
								<div class="right-col">
									<img src="<?php echo get_template_directory_uri(); ?>/library/images/x-white.png" height="168" width="100">
								</div>
							</div>
						</div>
					<?php } ?>
					<?php if (! in_category('views')) { ?>
					<h1 class="archive-title">
						<?php single_cat_title(); ?>
					</h1>
					<?php } ?>
					<ul>
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						<li>
							<?php if (! in_category('views')) { ?> <?php if(get_field('author')) { ?>
								<strong><?php the_field('author'); ?></strong> - 
							<?php } ?><?php } ?><a href="<?php the_permalink() ?>" rel="bookmark"><?php if(get_field('issue_number')) { ?><?php the_field('issue_number'); ?>,<?php } ?> <?php the_title(); ?></a>
							<?php if (in_category('views')) { } else { 
								if(get_field('pdf')) { ?>
									<a href="<?php the_field('pdf'); ?>" >
								<?php } else { ?>
									<a class="wpptopdfenh" title="Download PDF" href="<?php the_permalink() ?>?pdf=<?php the_id(); ?>" target="_blank" rel="noindex,nofollow">
								<?php } ?>
								<img src="<?php echo get_template_directory_uri(); ?>/library/images/pdf-icon.png" alt="Download PDF" /></a> 
							<?php } ?>
						</li>
					<?php endwhile; ?>
					</ul>
					
					<?php if (! in_category('views')) { ?>
					<section class="subscribe">
						<ul>
							<li>
								<div class="social">
									<a href="http://www.addthis.com/bookmark.php?v=250&amp;username=xa-4cbe6ce5170394e7" class="addthis_button_compact"> Share</a>
									<span class="addthis_separator">|</span>
									<a class="addthis_button_preferred_1"></a>
									<a class="addthis_button_preferred_2"></a>
									<a class="addthis_button_preferred_3"></a>
									<a class="addthis_button_preferred_4"></a>
								</div>
							</li>
							<li class="email">
								<img src="<?php echo get_template_directory_uri(); ?>/library/images/mail-icon.png" alt="Anthropoetics Email Subscription" /> <a href="http://feedburner.google.com/fb/a/mailverify?uri=AnthropoeticsTheJournalOfGenerativeAnthropology&amp;loc=en_US"><em>Anthropoetics</em> Email Subscription</a>
							</li>
							<li class="rss-library">
								<img src="<?php echo get_template_directory_uri(); ?>/library/images/brown-rss.png" height="12" width="12"> <a href="http://www.librarything.com/rss/recent/Anthropoetics"> Subscribe to LibraryThing</a></strong>
							</li>
							<li>
								<img src="<?php echo get_template_directory_uri(); ?>/library/images/anthro-home.png" width="16" height="15" /> <a href="<?php echo home_url(); ?>"><em>Anthropoetics</em> Home</a>
							</li>
							<li class="twitter">
								<img src="<?php echo get_template_directory_uri(); ?>/library/images/twitter.png" alt="Anthropoetics on Twitter" width="16" height="16"> <a href="http://www.twitter.com/Anthropoetics"><em>Anthropoetics</em> on Twitter</a>
							</li>
							<li class="anthro-rss">
								<img src="<?php echo get_template_directory_uri(); ?>/library/images/purple-rss.png" height="12" width="12"> <a href="/feed/">Subscribe to <em>Anthropoetics</em> RSS</a>
							</li>
							<li class="ga-rss">
								<img src="<?php echo get_template_directory_uri(); ?>/library/images/blue-rss.png" alt="RSS feed icon" height="12" width="12"> <a href="http://news.anthropoetics.ucla.edu/feed/">Subscribe to GA News Feed</a>
							</li>
						</ul>
					</section>
					<p class="aligncenter"><a href="/anthro/"> Return to <em>Anthropoetics</em> home page</a>
					<?php } ?>	
					
					<?php else : ?>

					<article id="post-not-found" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1>Page Not Found</h1>
						<section>
							<p>Sorry but nothing is available at this address. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a>, <p>Sorry but the page you are looking for is not here. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
						</section>
					</article>

					<?php endif; ?>

				</div>

<?php get_footer(); ?>